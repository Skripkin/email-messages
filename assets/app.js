var symbols = document.querySelector(".symbols"); 

window.addEventListener("mousemove", function(e) {
  var changeX = e.pageX / 80;
  var changeY = e.pageY / 80;

  symbols.style.transform = `translateX(${changeX}px) translateY(${changeY}px)`;
});