const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackInlineSourcePlugin = require("html-webpack-inline-source-plugin");
const path = require("path");
const fs = require("fs");
const autoprefixer = require('autoprefixer');

const PATH_TO_PAGES = "./assets/pages";

const files = fs.readdirSync(PATH_TO_PAGES);
const pages = ([]).map.call(files, path => {
  if (!path.match(/\.pug$/)) {
    return;
  }

  const fileName = path.replace(/\.pug$/, "");

  return new HtmlWebpackPlugin({
    inject: true,
    template: `${PATH_TO_PAGES}/${fileName}.pug`,
    filename: `${fileName}.html`,
    inlineSource: ".(js|css)$"
  });
});

module.exports = {
  entry: [
    "./assets/app.js",
    "./assets/import.scss"
  ],
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "[name].js",
  },
  module: {
    rules: [
      { 
        test: /\.pug$/,
        use: ["pug-loader"]
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          use: [{
              loader: "css-loader",
              options: {
                sourceMap: true,
                url: false
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                  plugins: [
                    autoprefixer()
                  ],
                  sourceMap: true
              }
            },
            {
              loader: "sass-loader",
              options: {
                sourceMap: true
              }
            }
          ]
        })
      }
    ]
  },
  plugins: [
    ...pages,
    new ExtractTextPlugin({
      filename: "css/style.css",
      allChunks: false
    }),
    new HtmlWebpackInlineSourcePlugin()
  ],
  devServer: {
    host: '0.0.0.0',
    port: 8888,
    contentBase: "./build",
    watchContentBase: true,
    lazy: false,
    open: true,
    progress: true
  }
};
